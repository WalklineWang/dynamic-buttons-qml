/*
 * Copyright (c) 2013 Simon HSU, bsebadmin(at)gmail.com
 * 
 * A simple sample to demonstrate how to add buttons dynamically in QML
 * The major problem is how to determine which button be clicked and show the related response
 * In here, I use objectName as the unique key to identify it.
 * 
 * 
 * 
 */

import bb.cascades 1.0
import bb.system 1.0

Page {
    Container {
        id: rootContainer
        horizontalAlignment: HorizontalAlignment.Center
        //Todo: fill me with QML
        Label {
            // Localized text with the dynamic translation and locale updates support
            text: qsTr("Dynamic QML Button") + Retranslate.onLocaleOrLanguageChanged
            textStyle.base: SystemDefaults.TextStyles.BigText
            textStyle.textAlign: TextAlign.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
    }

    attachedObjects: [
        ComponentDefinition {
            id: compDef

            Container {
                horizontalAlignment: HorizontalAlignment.Center
                property variant id_
                topPadding: 10;
                bottomPadding: 10;

                id: con
                Button {
                    id: bb
                    onClicked: {
                        console.log(bb.text);
                        myQmlToast.body = "Button " + id_ + " is clicked!"
                        myQmlToast.show();
                    }
                }
                function setID(i) {
                    id_= i;
                    con.objectName = "con"+i;
                    bb.text = "This is myButton " + i;
                }
            }
        },
        SystemToast {
            id: myQmlToast
        }
    ]

    onCreationCompleted: {
        for (var i = 0; i < 10; i ++) {
            var createdControl = compDef.createObject();
            createdControl.setID(i);
            rootContainer.add(createdControl);
        }
    }
}
